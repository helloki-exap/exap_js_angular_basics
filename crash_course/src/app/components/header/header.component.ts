import { Component, OnInit } from '@angular/core';
import { UiService } from "../../services/ui.service";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title: string = 'Task Tracker';
  show_add_task: boolean = false;
  subscription: Subscription;

  constructor(private ui_service: UiService, private router: Router) {
    this.subscription = this.ui_service.on_toggle_add_task().subscribe(
      (value) => (this.show_add_task = value)
    );
  }

  ngOnInit(): void {
  }

  toggle_add_task() {
    this.ui_service.toggle_add_task();
  }

  has_route(route: String): boolean {
    return this.router.url === route;
  }
}
