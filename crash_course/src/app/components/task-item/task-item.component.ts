import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../task';
import { faTimes } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {
  @Input() task!: Task;
  @Output() on_delete_task: EventEmitter<Task> = new EventEmitter<Task>();
  @Output() on_toggle_reminder: EventEmitter<Task> = new EventEmitter<Task>();
  faTimes = faTimes;

  constructor() { }

  ngOnInit(): void {
  }

  on_delete(task: Task) {
    this.on_delete_task.emit(task);
  }

  on_toggle(task: Task) {
    this.on_toggle_reminder.emit(task);
  }
}
