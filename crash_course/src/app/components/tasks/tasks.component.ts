import { Component, OnInit } from '@angular/core';
import { TaskService } from "../../services/task.service";
import { Task } from '../../task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  tasks: Task[] = [];

  constructor(private task_service: TaskService) { }

  ngOnInit(): void {
    this.task_service.get_tasks().subscribe((tasks) => this.tasks = tasks);
  }

  delete_task(task: Task) {
    this.task_service.delete_task(task)
      .subscribe(() =>
        this.tasks = this.tasks.filter((t) => t.id !== task.id)
      );
  }

  toggle_reminder(task: Task) {
    task.reminder = !task.reminder;
    this.task_service.update_task_reminder(task).subscribe();
  }

  add_task(task: Task) {
    this.task_service.add_task(task).subscribe(
      (task) => this.tasks.push(task)
    );
  }
}
