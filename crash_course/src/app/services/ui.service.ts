import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private show_add_task: boolean = false;
  private subject = new Subject<any>();

  constructor() { }

  toggle_add_task(): void {
    this.show_add_task = !this.show_add_task;
    this.subject.next(this.show_add_task);
  }

  on_toggle_add_task(): Observable<any> {
    return this.subject.asObservable();
  }
}
